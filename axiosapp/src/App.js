import React, { Component } from 'react';
import './App.css';
import axios from 'axios';


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      post: true,
    }
    this.posted = this.posted.bind(this);
  }
  posted(){
   axios.post('http://127.0.0.1:8000/routequestion/?format=json', {
      name: 'Axios yes Royales',
      email: 'axiosyes@test.com',
      question: 'this is my yes question'
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    }); 
    this.setState({post: !this.state.post});
    console.log("on click");
  }
  render() {
    return (
      <div className="App">
          <button onClick={this.posted}>post it</button>
      </div>
    );
  }
}

export default App;
